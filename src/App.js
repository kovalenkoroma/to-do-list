import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props){
      super(props);
      this.removeItem = this.removeItem.bind(this);
      this.state = {items: [{ index: 0, item: 'Buy vodka',
          completed: true},{ index: 1, item: 'Buy seledka',
          completed: true}]
      };
  }
  removeItem(index){
      // var items = this.state.items.splice(index, 1);
      // this.setState({items: items});
      fetch('/to-do-items'+ index, {method: 'POST'})
          .then(res => res.json())
          .then(items => console.log(items));
  }
  addItem(item){
    // let items = this.props.items.push(item);
  }
  componentDidMount() {
     fetch('/to-do-items')
        .then(res => res.json())
        .then(items => this.setState({ items }));
  }
  render() {
    return (
        <div>
            <h1>To Do List</h1>
            <ToDoList items={this.state.items} removeItem={this.removeItem}/>
        </div>
    );
  }
}

class ToDoList extends Component {

  render() {
    const listItems = this.props.items.map((item, index) =>
        <ToDoItem key={index} toDoItem={item} removeItem={this.props.removeItem} />
    );
    return (
        <ul className="to-do-list">
          {listItems}
        </ul>
    );
  }
}
class ToDoItem extends Component{
    constructor(props){
        super(props);
        this.removeItem = this.removeItem.bind(this)
    }
    removeItem(e) {
        var item = +e.currentTarget.dataset.item;
        this.props.removeItem(item);
    }
    render() {
        let item = this.props.toDoItem;
        return(
            <li className="item">
                <div className={(item.completed ? 'active' : 'inactive')}></div>
                <p>{item.item}</p>
                <button data-item={item.index} onClick={this.removeItem}>Delete Item</button>
            </li>
        )
    }

}

export {App};
